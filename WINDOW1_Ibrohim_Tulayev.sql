WITH RankedCustomers AS (
    SELECT
        CustomerID,
        SalesChannel,
        TotalSales,
        RANK() OVER (PARTITION BY SalesChannel, OrderYear ORDER BY TotalSales DESC) AS SalesRank
    FROM
        Sales
    WHERE
        OrderYear IN (1998, 1999, 2001)
)
SELECT
    CustomerID,
    SalesChannel,
    OrderYear,
    TotalSales
FROM
    RankedCustomers
WHERE
    SalesRank <= 300
ORDER BY
    SalesChannel, OrderYear, SalesRank;
